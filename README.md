# Django Project Template

## Working Environment

I recommend using virtualenv to separate the dependencies of the project from your system's python environment.  

### Virtual Enviroment

#### Install virtualenv, pip and virtualenvwrapper

```
    $ sudo apt-get install python-virtualenv    
    $ sudo apt-get install python-pip
    $ pip install virtualenvwrapper
    $ export WORKON_HOME=/opt/environments
    $ source /usr/local/bin/virtualenvwrapper.sh
```

#### Create and activate an environment for your application

##### I keep all my web apps and environments in the /opt/ directory
```
    $ mkvirtualenv project_name
```

#### Installing Django

##### To install Django in the new virtual environment, run the following command:

```
    $ pip install django
```

#### Creating your Project

```
$ django-admin.py startproject --template=https://bitbucket.org/qncove/django-project-template/get/master.zip --extension=py,rst,html project_name
```

#### Installing application requirements

##### Development
```
    $ pip install -r requirements/local.txt
```


##### Running the server

```
    $ python manage.py runserver --settings=project_name.settings.local
```

